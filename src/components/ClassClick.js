import React, { Component } from 'react'

export class ClassClick extends Component {
clickHandler()
{
    console.log('Button click')
}

    render() {
        return (
            <div>
              <button onClick={this.clickHandler}>Click meeee</button>  
            </div>
        )
    }
}

export default ClassClick
