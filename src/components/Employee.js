import React, { Component } from 'react'
import Department from './Department'
import Salary from './Salary'
class Employee extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
          name:"Rupali",
          workingDays:28,
          workingHours:8,
          ChargePerHours:53,
          TotalSalary:''
        }
    }
    
    render() {
        return (
            <div>
                <h1>Employee Details</h1>
                <h3>Employee Name:{this.state.name}</h3>
                <h3>Employee workingDays:{this.state. workingDays}</h3>
                <h3>Employee workingHours:{this.state.workingHours}</h3>
                <h3>Employee ChargePerHours:{this.state.ChargePerHours}</h3>
                <h3>Employee TotalSalary:{this.state.TotalSalary}</h3>

                <Department name={this.state.name} />
                <Salary name={this.state.name} />
            </div>
        )
    }
}

export default Employee
