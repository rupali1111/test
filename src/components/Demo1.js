import React from 'react';
import './Demo1.css';

function Demo1()
{
    return(
        <div>

        <p class="para">
If you’re interested in playing around with React,
 you can use an online code playground. Try a Hello
  World template on CodePen, CodeSandbox, Glitch, or
  Stackblitz.

If you prefer to use your own text editor, you can also download this HTML file, edit it, and open it from the local filesystem in your browser. It does a slow runtime code transformation,
 so we’d only recommend using this for simple demo
</p>

        </div>

    );

}
export default Demo1;