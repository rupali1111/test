import React, { Component } from 'react'

export class ContactForm extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
          name:'',
          email:'',
          city:'',
          message:''  
        }
    }
    
    render() {
        return (
            <div>
               <h2>Contact Form</h2> 
               <form>
               <div>
            Name:
            <input type="text" name="name" value={this.state.name} />
               </div>
               <div>
               <button type="submit">send message</button>
               </div>
               </form>
            </div>
        )
    }
}

export default ContactForm
