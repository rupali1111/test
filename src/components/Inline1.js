import React, { Component } from 'react'

class Inline1 extends Component {
    render() {
const btnStyle = {
    backgroundColor:"pink",
    color:"blue"
}
        return (
            <div>
              <button style={btnStyle}>Button</button>  
            </div>
        )
    }
}

export default Inline1;
