import React, { Component } from 'react'

 class Employee1 extends Component {
    render() {
        return (
            <div>
             <h1 className={this.props.color}>Employee1 Details</h1>   
             <h3>Name:{this.props.name}</h3>
             <h3>Department:{this.props.depart}</h3>
             <h3>Salary:{this.props.salary}</h3>
            </div>
        )
    }
}

export default Employee1
