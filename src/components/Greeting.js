import React from 'react';
import ReactDOM from 'react-dom';
import UserGreeting from '.UserGreeting';
import GuestGreeting from 'GuestGreeting';



function Greeting(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
}
export default Greeting;

// ReactDOM.render(
//   // Try changing to isLoggedIn={true}:
//   <Greeting isLoggedIn={false} />,
//   document.getElementById('root')
// );